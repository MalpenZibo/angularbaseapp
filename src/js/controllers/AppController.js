( function()
{
  'use strict';

  angular.module( 'BaseApp.controllers' )
  .controller( 'AppCtrl', [ 
    '$rootScope',
    '$log',
    '$mdSidenav',
    'menu',
    function( $rootScope, $log, $mdSidenav, menu )
    {
      var self = this;

      //functions for menu-link and menu-toggle
      self.menu = menu;
      self.toggleList = toggleList;
      self.isOpen = isOpen;
      self.toggleOpen = toggleOpen;
      self.currentSection = null;

      function toggleList() 
      {
        $mdSidenav( 'left' ).toggle();
      }

      function isOpen( section ) 
      {
        return menu.isSectionSelected( section );
      }

      function toggleOpen( section ) 
      {
        menu.toggleSelectSection( section );
      }
    } 
  ] );
} )();