Angular Base App
==============

This project is an application skeleton for a typical AngularJS web app with Material Design.

Getting Started
--------------

- get Node.js
- "npm install" inside project folder
- "gulp build" for build the project ( --production for production build )
- "gulp autobuild" for automatically build project when a source file is saved ( only with develop build )
- "gulp connect" for start a local webserver(localhost:8888) on develop build ( --production for a local webserver(localhost:9999) on production build)
