( function()
{
  'use strict';

  // This CSS class-based directive controls the pre-bootstrap loading screen. By
  // default, it is visible on the screen; but, once the application loads, we'll
  // fade it out and remove it from the DOM.
  angular.module( 'common.directives' )
  .directive(
    "appLoading",
    function( $animate ) 
    {
      // Return the directive configuration.
      return(
      {        
        restrict: 'A',
        link: function( scope, element, attributes )
        {
          // Due to the way AngularJS prevents animation during the bootstrap
          // I have create a fadeout animation

          function fadeOut( elem, ms, delegate )
          {
            if( ! elem )
              return;

            if( ms )
            {
              var opacity = 1;
              var timer = setInterval( 
                function() 
                {
                  opacity -= 50 / ms;
                  if( opacity <= 0 )
                  {
                    clearInterval( timer );
                    opacity = 0;

                    elem.css( "display", "none" );
                    elem.css( "visibility", "hidden" );

                    delegate();
                  }
                  elem.css( "opacity", opacity );
                  elem.css( "filter", "alpha(opacity=" + opacity * 100 + ")" );

                }, 50 
              );
            }
            else
            {
              elem.css( "opacity", 0 );
              elem.css( "filter", "alpha(opacity=0)" );
              elem.css( "display", "none" );
              elem.css( "visibility", "hidden" );
            }
          }
              
          fadeOut( element, 400,
            function()
            {
              var stylesheets = document.getElementsByTagName( 'link' ), i, sheet, rel, href;

              for( i in stylesheets ) 
              {
                sheet = stylesheets[i];
                if( typeof sheet == 'object' )
                {
                  rel = sheet.getAttribute( 'rel' );
                  href = sheet.getAttribute( 'href' );

                  if( rel !== null && rel.toLowerCase() == 'stylesheet' && href !== null && href.toLowerCase() == 'app-loading.css' )
                    sheet.parentNode.removeChild( sheet );
                }
              }

              // Remove the root directive element.
              element.remove();
            }
          );
        }
      } );
    }
  );
})();