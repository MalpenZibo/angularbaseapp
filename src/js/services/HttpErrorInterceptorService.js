( function()
{
  'use strict';

  angular.module( 'common.services' )
  .factory( 'httpErrorInterceptor', 
    function( $q, $location )
    {
      return {
        'responseError': function( rejection ) 
        {
          switch( rejection.status )
          {
            case 400:
            case 401:
            case 402:
            case 403:
            case 404:
            case 500:
            case 501:
              $location.path( '/error/' + rejection.status );
              break;
          }
          
          return $q.reject( rejection );
        }
      };
    }
  );
} )();