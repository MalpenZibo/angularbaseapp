( function()
{
  'use strict';

  angular.module( 'common.directives' )
    .directive( 'menuLink', function() 
    {
      return {
        scope: {
          section: '='
        },
        templateUrl: 'partials/menu-link.html',
        link: function( $scope, $element ) 
        {
          var controller = $element.parent().controller();

          $scope.focusSection = function() 
          {
            controller.currentSection = $scope.section;
          };

          $scope.isSelected = function() 
          {
            return controller.currentSection == $scope.section;
          };
        }
      };
    } );
} )();