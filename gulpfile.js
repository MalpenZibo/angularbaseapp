// gulp
var gulp = require( 'gulp' );

// plugins
var connect = require( 'gulp-connect' );
var jshint = require( 'gulp-jshint' );
var uglify = require( 'gulp-uglify' );
var minifyCSS = require( 'gulp-minify-css' );
var minifyHTML = require( 'gulp-minify-html' );
var del = require( 'del' );
var concatFile = require( 'gulp-concat' );
var mainBowerFiles = require( 'main-bower-files' );
var filter = require( 'gulp-filter' );
var argv = require( 'yargs' ).argv;
var gulpif = require( 'gulp-if' );
var sourcemaps = require( 'gulp-sourcemaps' );
var sass = require( 'gulp-sass' );
var streamqueue = require('streamqueue');
var order = require( 'gulp-order' );
var rename = require( 'gulp-rename' );
var exists = require( 'path-exists' ).sync;

// Replace files by their minified version when possible
var bowerWithMin = mainBowerFiles().map( function( path, index, arr ) 
{
  var newPath = path.replace( /.([^.]+)$/g, '.min.$1' );
  return exists( newPath ) ? newPath : path;
} );

// tasks
gulp.task( 'lint', function() 
{
  gulp.src( ['./src/js/**/*.js'] )
  .pipe( jshint() )
  .pipe( jshint.reporter( 'default' ) );
  //.pipe( jshint.reporter( 'fail' ) );
} );

gulp.task( 'clean', function() 
{
  if( argv.production )
    del( ['./dist/production'] );
  else 
    del( ['./dist/develop'] );
} );

gulp.task( 'compile-styles', function() 
{
  var cssFiles = ['./src/styles/**/*.css'];
  var opts = 
  { 
    comments: true, 
    keepSpecialComments: false
  };

  streamqueue( { objectMode: true },
    gulp.src( mainBowerFiles() )
      .pipe( filter( '**/*.css' ) ),
    gulp.src( './src/styles/**/*.css' )
      .pipe( filter( ['*', '!**/*.standalone.css'] ) ),
    gulp.src( './src/styles/**/*.scss' )
      .pipe( filter( ['*', '!**/*.standalone.scss'] ) )
      .pipe( gulpif( !argv.production, sourcemaps.init() ) )
      .pipe( 
        sass( 
        { 
          onError: function( e ) { console.log( e ); } 
        } ) 
      )
      .pipe( gulpif( !argv.production, sourcemaps.write() ) )
  )
  .pipe( gulpif( !argv.production, sourcemaps.init() ) )
  .pipe( concatFile( 'style.css' ) ) 
  .pipe( gulpif( argv.production, minifyCSS( opts ) ) )
  .pipe( gulpif( !argv.production, sourcemaps.write() ) )
  .pipe( gulpif( argv.production, gulp.dest( './dist/production' ), gulp.dest( './dist/develop' ) ) );

  streamqueue( { objectMode: true },
    gulp.src( './src/styles/**/*.css' ).pipe( filter( '**/*.standalone.css' ) ),
    gulp.src( './src/styles/**/*.scss' ).pipe( filter( '**/*.standalone.scss' ) )
    .pipe( gulpif( !argv.production, sourcemaps.init() ) )
    .pipe( 
      sass( 
      { 
        onError: function( e ) { console.log( e ); } 
      } ) 
    )
    .pipe( gulpif( !argv.production, sourcemaps.write() ) )
  )
  .pipe( gulpif( argv.production, minifyCSS( opts ) ) )
  .pipe( rename( 
    function( path )
    {
      path.basename = path.basename.replace( '.standalone', '' );
    } )
  )
  .pipe( gulpif( argv.production, gulp.dest( './dist/production' ), gulp.dest( './dist/develop' ) ) );
} );

gulp.task( 'compile-js', function() 
{
  var jsFiles = ['./src/js/App.js', './src/js/**/*.js'];

  streamqueue( { objectMode: true },
    gulp.src( bowerWithMin )
      .pipe( filter( '**/*.js' ) )
      .pipe( uglify( { compress: false, mangle: false } ) ),
    gulp.src( jsFiles )
      .pipe( gulpif( argv.production, uglify( { mangle: false } ) ) )
  )
  .pipe( gulpif( !argv.production, sourcemaps.init() ) )
  .pipe( concatFile( 'app.js' ) )
  .pipe( gulpif( !argv.production, sourcemaps.write() ) ) 
  .pipe( gulpif( argv.production, gulp.dest( './dist/production' ), gulp.dest( './dist/develop' ) ) ); 
} );

gulp.task( 'compile-html-files', function () 
{
  var opts = 
  {
    conditionals: true,
    spare: true
  };

  gulp.src( ['./src/index.html', './src/views/**/*.html', './src/partials/**/*.html'], { base: './src/'} )
  .pipe( gulpif( argv.production, minifyHTML( opts ) ) )
  .pipe( gulpif( argv.production, gulp.dest( './dist/production' ), gulp.dest( './dist/develop' ) ) );
} );

gulp.task( 'copy-assets', function () 
{
  gulp.src( './src/assets/**' )
  .pipe( gulpif( argv.production, gulp.dest( './dist/production/assets' ), gulp.dest( './dist/develop/assets' ) ) );
} );

gulp.task( 'connect', function () 
{
  if( argv.production )
  {
    connect.server( 
    {
      root: 'dist/production',
      port: 9999
    } );
  }
  else
  {
    connect.server( 
    {
      root: 'dist/develop',
      port: 8888
    } );  
  }  
} );

// default task
gulp.task( 'default', ['lint', 'connect'] );

// build task
gulp.task( 'build', ['lint', 'compile-styles', 'compile-js', 'compile-html-files', 'copy-assets'] );

//automatic build
gulp.task( 'autobuild', function()
{
  gulp.watch( './src/**/*.*', ['build'] );
} );