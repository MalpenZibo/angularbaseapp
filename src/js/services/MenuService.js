( function()
{
  'use strict';

  angular.module( 'common.services' )
  .factory( 'menu', 
  [
    '$location',
    function( $location )
    {
      var sections = 
      [ 
        { 
          name: 'Getting Started',
          route: 'Login',
          type: 'link'
        }, 
        { 
          name: 'Beers',
          type: 'toggle',
          pages: 
          [
            {
              name: 'IPAs',
              type: 'link',
              route: 'Test',
              icon: 'fa fa-group'
            }, 
            {
              name: 'Not Found',
              route: 'blablabla',
              type: 'link',
              icon: 'fa fa-map-marker'
            },
            {
              name: 'Wheat',
              route: 'Test',
              type: 'link',
              icon: 'fa fa-plus'
            }
          ]
        },  
        {
          name: 'Munchies',
          type: 'toggle',
          pages: 
          [
            {
              name: 'Login',
              type: 'link',
              route: 'Login',
              icon: 'fa fa-group'
            }, 
            {
              name: 'Banana Chips',
              route: 'Test',
              type: 'link',
              icon: 'fa fa-map-marker'
            },
            {
              name: 'Donuts',
              route: 'Test',
              type: 'link',
              icon: 'fa fa-map-marker'
            }
          ]
        }
      ];

      var self;

      self = {
        sections: sections,

        toggleSelectSection: function( section ) 
        {
          self.openedSection = (self.openedSection === section ? null : section);
        },
        isSectionSelected: function( section ) 
        {
          return self.openedSection === section;
        }
      };

      return self;
    } 
  ] );
} )();