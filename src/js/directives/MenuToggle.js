( function()
{
  angular.module( 'common.directives' )
    .directive( 'menuToggle', [ '$timeout', function( $timeout ) 
    {
      return {
        scope: {
          section: '='
        },
        templateUrl: 'partials/menu-toggle.html',
        link: function( $scope, $element ) 
        {
          var controller = $element.parent().controller();

          $scope.isOpen = function() 
          {
            return controller.isOpen( $scope.section );
          };
          $scope.toggle = function() 
          {
            controller.toggleOpen( $scope.section );
          };

          $scope.$watch(
            function () 
            {
              return controller.isOpen( $scope.section );
            },
            function( open ) 
            {
              var $list = $element.find( 'md-list' );
              var targetHeight = open ? getTargetHeight() : 0;
              
              $timeout( function () 
              {
                $list.css( { height: targetHeight + 'px' } );
              }, 0, false);

              function getTargetHeight() 
              {
                var targetHeight;
                $list.addClass( 'no-transition' );
                $list.css( 'height', '' );
                targetHeight = $list.prop( 'clientHeight' );
                $list.css( 'height', 0 );
                $list.removeClass( 'no-transition' );
                return targetHeight;
              }
            }
          );
        }
      };
    } ] );
} )();