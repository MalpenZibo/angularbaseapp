( function()
{
  'use strict';

  angular.module( 'BaseApp.controllers' )
  .controller( 'LoginCtrl',
    function( $scope, $auth )
    {
      $scope.authenticate = function( provider ) 
      {
        $auth.authenticate( provider );
      };
    } 
  );
} )();