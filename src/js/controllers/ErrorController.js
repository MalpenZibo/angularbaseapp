( function()
{
  'use strict';

  angular.module( 'BaseApp.controllers' )
  .controller( 'ErrorCtrl',
    function( $scope, $routeParams )
    {
      $scope.errorCode = $routeParams.errorCode;
    } 
  );
} )();