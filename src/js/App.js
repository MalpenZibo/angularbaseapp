( function()
{
  'use strict';

  angular.module( 'common.services', [] );
  angular.module( 'BaseApp.controllers', ['common.directives'] );
  angular.module( 'common.directives', ['common.services', 'ngAnimate'] );

  angular.module( 'BaseApp', [
    'BaseApp.controllers',
    'common.services',
    'ngAnimate',
    'ngRoute',
    'ngMaterial',
    'ngAria',
    'satellizer'
  ] )  
  .config( ['$routeProvider', '$httpProvider',
    function( $routeProvider, $httpProvider ) 
    {
      $httpProvider.interceptors.push( 'httpErrorInterceptor' );

      $routeProvider.
        when( '/', {
          templateUrl: 'views/login.view.html',
          controller: 'LoginCtrl as vm'
        } )
        .when( '/:controller', {
          templateUrl: function( urlAttr ) 
          { 
            return 'views/' + urlAttr.controller.toLowerCase() + '.view.html'; 
          },
        } )
        .when( '/error/:errorCode', {
          templateUrl: 'views/error.view.html',
          controller: 'ErrorCtrl as vm'
        } )
        .otherwise({
          redirectTo: '/'
        });
    } ] 
  )
  .config( 
    function( $authProvider )
    {

    } 
  )
  .config( ['$logProvider',
    function( $logProvider )
    {
      $logProvider.debugEnabled( true );
    } ]
  )
  //take all whitespace out of string
  .filter( 'nospace', 
    function() 
    {
      return function( value ) 
      {
        return (!value) ? '' : value.replace( / /g, '' );
      };
    } 
  )
  //replace uppercase to regular case
  .filter( 'humanizeDoc', 
    function() 
    {
      return function( doc ) 
      {
        if( !doc ) return;
        if( doc.type === 'directive' ) 
        {
          return doc.name.replace(/([A-Z])/g, function( $1 ) 
          {
            return '-' + $1.toLowerCase();
          } );
        }

        return doc.label || doc.name;
      };
    } 
  );

  // SIMULATING NETWORK LATENCY AND LOAD TIME. We haven't included the ngApp
  // directive since we're going to manually bootstrap the application. This is to
  // give the page a delay, which it wouldn't normally have with such a small app.
  setTimeout(
    function asyncBootstrap() 
    {
      angular.bootstrap( document, [ "BaseApp" ] );
    },
    ( 2 * 1000 )
  );


} )();
